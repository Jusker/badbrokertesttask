using BadBrokerTestTask.Models.UI.Calculators;
using BadBrokerTestTask.Services.Calculators.SignleCurrency;

namespace BadBrokerTestTask.Tests.SingleCurrency
{
    public class SignleCurrencyCalculator_Testing
    {
        private readonly ISingleCurrencyCalculator singleCurrencyCalculator;
        public SignleCurrencyCalculator_Testing() {
            singleCurrencyCalculator = new SingleCurrencyCalculator();
        }


        [Theory]
        [InlineData(100, 1, "2014-12-16", "2014-12-22", 127.24)]
        public void Example_1(decimal startValue, decimal tax, string expectedBuyDate, string expectedSellDate, decimal expectedValue)
        {
            var inputRates = new List<SignleCurrencyModel> {
                new SignleCurrencyModel { Date= new DateTime(2014,12,15), Value = 60.17m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,16), Value = 72.99m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,17), Value =66.01m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,18), Value =61.44m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,19), Value = 59.79m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,20), Value = 59.79m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,21), Value = 59.79m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,22), Value =54.78m },
                new SignleCurrencyModel { Date=new DateTime(2014,12,23), Value =54.80m },

            };

            var result = singleCurrencyCalculator.FindBestMatch(inputRates, startValue, tax);

            Assert.Equal(DateTime.Parse(expectedBuyDate), result.BuyDate);
            Assert.Equal(DateTime.Parse(expectedSellDate), result.SellDate);
            Assert.Equal(expectedValue, result.Revenue, 2);
        }


        [Theory]
        [InlineData(50, 1, "2012-01-05", "2012-01-07", 55.14)]
        public void Example_2(decimal startValue, decimal tax, string expectedBuyDate, string expectedSellDate, decimal expectedValue)
        {
            var inputRates = new List<SignleCurrencyModel> {
                new SignleCurrencyModel { Date= new DateTime(2012,1,5), Value = 40.0m },
                new SignleCurrencyModel { Date=new DateTime(2012,1,7), Value = 35.0m },
                new SignleCurrencyModel { Date=new DateTime(2012,1,19), Value =30.0m },
            };

            var result = singleCurrencyCalculator.FindBestMatch(inputRates, startValue, tax);

            Assert.Equal(DateTime.Parse(expectedBuyDate), result.BuyDate);
            Assert.Equal(DateTime.Parse(expectedSellDate), result.SellDate);
            Assert.Equal(expectedValue, result.Revenue, 2);
        }
    }
}