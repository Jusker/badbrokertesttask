﻿using BadBrokerTestTask.DataBase.Models.DAL.DTO;
using BadBrokerTestTask.DataBase.Models.DAL.Repository;
using BadBrokerTestTask.Models.UI;
using BadBrokerTestTask.Services.RateGetters;
using BadBrokerTestTask.Utils;
using Moq;

namespace BadBrokerTestTask.Tests.RateGetter
{
    public class RateGetterWithCache_Testing
    {
        
        [Fact]
        public void AllDataStoresInCache() {

            var startValues = new
            {
                DateStart = new DateTime(2022, 1, 1),
                DateEnd = new DateTime(2022, 1, 2),
                BaseCurrency = "USD",
                Symbols = new string[] { "RUB" },
            };

            var resultGetCaches = new List<Cache>
            {
                new Cache { BaseCurrency = "USD", Currency = "RUB", Date = startValues.DateStart, Value = 1 },
                new Cache { BaseCurrency = "USD", Currency = "RUB", Date = startValues.DateEnd, Value = 2 },
            };

            var mockDbCacheGetter = new Mock<IDbCacheGetter>();
            mockDbCacheGetter.Setup(r => r.GetExistsRows(
                startValues.DateStart,
                startValues.DateEnd,
                startValues.BaseCurrency,
                startValues.Symbols
                ))
                .Returns(resultGetCaches);

            var mockDbCacheCreator = new Mock<IDbCacheCreator>();
            var mockRateSimpleGetter = new Mock<IRateSimpleGetter>();
            var mockDateRangeJoiner = new Mock<IDateRangeJoiner>();

            var dateRangeJoiner = new DateRangeJoiner();

            IApiLayerWithCacheGetter apiLayerWithCacheGetter = new ApiLayerWithCacheGetter(
                mockDbCacheGetter.Object,
                mockDbCacheCreator.Object,
                mockRateSimpleGetter.Object,
                dateRangeJoiner
                );

            var result = apiLayerWithCacheGetter.GetAsync(startValues.DateStart, startValues.DateEnd, startValues.BaseCurrency, startValues.Symbols).Result;


            Assert.Equal(0, mockRateSimpleGetter.Invocations.Count);
            Assert.Equal(1, mockDbCacheGetter.Invocations.Count);
            Assert.Equal(2, result.Count);
            Assert.Single(result.Where(e => e.DateTime == startValues.DateStart && e.Value == 1));
            Assert.Single(result.Where(e => e.DateTime == startValues.DateEnd && e.Value == 2));

        }

        [Fact]
        public void PartDataStoresInCache() {

            var startValues = new
            {
                DateStart = new DateTime(2022, 1, 1),
                DateEnd = new DateTime(2022, 1, 2),
                BaseCurrency = "USD",
                Symbols = new string[] { "RUB" },
            };

            var resultGetCaches = new List<Cache>
            {
                new Cache { BaseCurrency = "USD", Currency = "RUB", Date = startValues.DateStart, Value = 1 },
            };

            var resultGetAPI = new List<Rate>
            {
                new Rate { Currency = "RUB", DateTime = startValues.DateEnd, Value = 2 },
            };

            var mockDbCacheGetter = new Mock<IDbCacheGetter>();
            mockDbCacheGetter.Setup(r => r.GetExistsRows(
                startValues.DateStart,
                startValues.DateEnd,
                startValues.BaseCurrency,
                startValues.Symbols
                ))
                .Returns(resultGetCaches);

            var mockDbCacheCreator = new Mock<IDbCacheCreator>();
            var mockRateSimpleGetter = new Mock<IRateSimpleGetter>();
            mockRateSimpleGetter.Setup(r => r.GetAsync(
                startValues.DateEnd,
                startValues.DateEnd,
                startValues.BaseCurrency,
                startValues.Symbols
                ))
                .ReturnsAsync(resultGetAPI);

            var mockDateRangeJoiner = new Mock<IDateRangeJoiner>();

            var dateRangeJoiner = new DateRangeJoiner();

            IApiLayerWithCacheGetter apiLayerWithCacheGetter = new ApiLayerWithCacheGetter(
                mockDbCacheGetter.Object,
                mockDbCacheCreator.Object,
                mockRateSimpleGetter.Object,
                dateRangeJoiner
                );

            var result = apiLayerWithCacheGetter.GetAsync(startValues.DateStart, startValues.DateEnd, startValues.BaseCurrency, startValues.Symbols).Result;

            Assert.Equal(1, mockRateSimpleGetter.Invocations.Count);
            Assert.Equal(1, mockDbCacheGetter.Invocations.Count);
            Assert.Equal(1, mockDbCacheCreator.Invocations.Count);
            Assert.Equal(2, result.Count);
            Assert.Single(result.Where(e => e.DateTime == startValues.DateStart && e.Value == 1));
            Assert.Single(result.Where(e => e.DateTime == startValues.DateEnd && e.Value == 2));
        }
    }
}
