﻿using BadBrokerTestTask.Utils;

namespace BadBrokerTestTask.Tests.Utils
{
    public class JoinDateRange_Testing
    {

        private readonly IDateRangeJoiner dateRangeJoiner;

        public JoinDateRange_Testing() {

            dateRangeJoiner = new DateRangeJoiner();
        }

        [Fact]
        public void Join_Success() {
            var dates = new List<DateTime> {
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 2),
                new DateTime(2022, 1, 3),
            };


            var result = dateRangeJoiner.JoinDateList(dates);


            Assert.Single(result);
            Assert.Equal((new DateTime(2022, 1, 1), new DateTime(2022, 1, 3)), result.First());

        }


        [Fact]
        public void SomeJoin_Success()
        {
            var dates = new List<DateTime> {
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 2),
                new DateTime(2022, 1, 3),
                new DateTime(2022, 1, 5),
                new DateTime(2022, 1, 6),
            };


            var result = dateRangeJoiner.JoinDateList(dates);


            Assert.Equal(2, result.Count);
            Assert.Equal((new DateTime(2022, 1, 5), new DateTime(2022, 1, 6)), result.Last());

        }

        [Fact]
        public void Doubles_Success()
        {
            var dates = new List<DateTime> {
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 1),
                new DateTime(2022, 1, 1),
            };


            var result = dateRangeJoiner.JoinDateList(dates);


            Assert.Single(result);
            Assert.Equal((new DateTime(2022, 1, 1), new DateTime(2022, 1, 1)), result.Single());

        }


        [Fact]
        public void IncorrectEmptyInputList()
        {
            var dates = new List<DateTime> {
            };

            var result = dateRangeJoiner.JoinDateList(dates);

            Assert.Empty(result);

        }
    }
}
