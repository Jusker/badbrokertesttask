﻿namespace BadBrokerTestTask.Utils
{
    /// <summary>
    /// Functionality for combining non-breaking date sets into ranges
    /// </summary>
    public interface IDateRangeJoiner
    {
        List<(DateTime, DateTime)> JoinDateList(IEnumerable<DateTime> dates);
    }
}
