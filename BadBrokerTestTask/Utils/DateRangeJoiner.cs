﻿namespace BadBrokerTestTask.Utils
{
    /// <summary>
    /// Implementation <see cref="IDateRangeJoiner"/>
    /// </summary>
    public class DateRangeJoiner: IDateRangeJoiner
    {

        public List<(DateTime, DateTime)> JoinDateList(IEnumerable<DateTime> dates) {

            List<(DateTime, DateTime)> result = new List<(DateTime, DateTime)>();

            if (!dates.Any()) return result;

            var orderedDates = dates.OrderBy(e => e).ToArray();

            var previousDate = orderedDates.First();
            var lastDate = orderedDates.Last();

            for (int i = 0; i < orderedDates.Length; i++) {
                if (previousDate == orderedDates[i] || orderedDates[i - 1].AddDays(1) == orderedDates[i]) {
                    continue;
                }
                result.Add((previousDate, orderedDates[i-1]));
                previousDate = orderedDates[i];
            }

            result.Add((previousDate, lastDate));

            return result;
        }
    }
}
