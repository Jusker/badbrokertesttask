﻿using BadBrokerTestTask.Models.UI.Calculators;

namespace BadBrokerTestTask.Services.Calculators.SignleCurrency
{
    /// <summary>
    /// Functionality for calculating the best day/value set among the same currency
    /// </summary>
    public interface ISingleCurrencyCalculator
    {
        /// <summary>
        /// Find best match
        /// </summary>
        BestMathInfo FindBestMatch(IEnumerable<SignleCurrencyModel> values, decimal baseValue, decimal tax);
    }
}
