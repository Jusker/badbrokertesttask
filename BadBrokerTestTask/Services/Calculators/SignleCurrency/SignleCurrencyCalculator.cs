﻿using BadBrokerTestTask.Models.UI.Calculators;

namespace BadBrokerTestTask.Services.Calculators.SignleCurrency
{
    /// <summary>
    /// Implementation <see cref="ISingleCurrencyCalculator"/>
    /// </summary>
    public class SingleCurrencyCalculator : ISingleCurrencyCalculator
    {

        ///<inheritdoc/>
        public BestMathInfo FindBestMatch(IEnumerable<SignleCurrencyModel> values, decimal baseValue, decimal tax) {

            if (values == null || !values.Any())
                throw new ArgumentException($"A non-empty array must be passed to the {nameof(SingleCurrencyCalculator.FindBestMatch)} method!");

            if (values.Any(e => e.Value == 0))
                throw new ArgumentException("One of the array values is 0, which will result in an error when dividing!");

            if (values.Count() == 1) {
                var single = values.Single();
                
                return new BestMathInfo { 
                    BuyDate = single.Date,
                    SellDate = single.Date,
                    Revenue = 0,
                };
            } 

            var bestMatch = values
                .SelectMany(e => values, (a, b) => new { buy = a, sell = b })
                .Where(s => s.buy.Date < s.sell.Date)
                .Select(e => new {
                    buy = e.buy,
                    sell = e.sell,
                    revenue = (e.buy.Value * baseValue / e.sell.Value) - (tax * (e.sell.Date - e.buy.Date).Days)
                })
                .OrderByDescending(e => e.revenue)
                .First();
            
            var result = new BestMathInfo { 
                BuyDate = bestMatch.buy.Date,
                SellDate = bestMatch.sell.Date,
                Revenue = bestMatch.revenue,
            };

            return result;
        }
    }
}
