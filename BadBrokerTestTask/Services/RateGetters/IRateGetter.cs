﻿using BadBrokerTestTask.Models.UI;

namespace BadBrokerTestTask.Services.RateGetters
{
    public interface IRateGetter
    {
        Task<List<Rate>> GetAsync(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols);
    }
}
