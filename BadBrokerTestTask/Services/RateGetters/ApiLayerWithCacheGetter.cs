﻿using BadBrokerTestTask.DataBase.Models.DAL.Repository;
using BadBrokerTestTask.DataBase.Models.UI;
using BadBrokerTestTask.Models.UI;
using BadBrokerTestTask.Utils;

namespace BadBrokerTestTask.Services.RateGetters
{
    /// <summary>
    /// Implementation <see cref="IApiLayerWithCacheGetter"/>
    /// </summary>
    public class ApiLayerWithCacheGetter : IApiLayerWithCacheGetter
    {
        private readonly IDbCacheGetter _dbCacheGetter;
        private readonly IDbCacheCreator _dbCacheCreator;
        private readonly IRateSimpleGetter _baseRateGetter;
        private readonly IDateRangeJoiner _dateRangeJoiner;

        public ApiLayerWithCacheGetter(
            IDbCacheGetter dbCacheGetter,
            IDbCacheCreator dbCacheSetter,
            IRateSimpleGetter baseRateGetter,
            IDateRangeJoiner dateRangeJoiner
            )
        {
            _dbCacheGetter = dbCacheGetter;
            _dbCacheCreator = dbCacheSetter;
            _baseRateGetter = baseRateGetter;
            _dateRangeJoiner = dateRangeJoiner;
        }

        public async Task<List<Rate>> GetAsync(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols)
        {
            if (dateEnd < dateStart)
                throw new ArgumentException("End date is greater than start date. Maybe swap dates?");


            //Get actual cache
            var ratesInCache = _dbCacheGetter.GetExistsRows(dateStart, dateEnd, baseCurrency, symbols)
                .Select(r => new Rate
                {
                    Currency = r.Currency,
                    DateTime = r.Date,
                    Value = r.Value,
                });

            //Get all days in range
            var dates = Enumerable.Range(0, (dateEnd - dateStart).Days + 1)
                .Select(offset => dateStart.AddDays(offset))
                .ToList();

            //Get all days which have not data's or not enough data's
            //Days that have entries but don't have the right characters will be requested again
            var datesForSerach = dates
                .Where(date =>
                    !symbols.All(sym => ratesInCache.Where(rate => rate.Currency == sym && rate.DateTime == date).Any())
                );

            //Collect into groups. For each group, we make a request to api
            var groups = _dateRangeJoiner.JoinDateList(datesForSerach)
                .Select(async group => await _baseRateGetter.GetAsync(group.Item1, group.Item2, baseCurrency, symbols)).ToList();

            await Task.WhenAll(groups);

            //Finding a list of what needs to be stored in the cache
            var newRates = groups.SelectMany(e => e.Result)
                .Where(r => !ratesInCache.Any(t => t.Currency == r.Currency && t.DateTime == r.DateTime));

            //If there are lines to write, put them in the cache
            if (newRates.Any())
                await _dbCacheCreator.InsertAsync(newRates.Select(e => new CacheCreateRow
                {
                    Currency = e.Currency,
                    DateTime = e.DateTime,
                    Value = e.Value,
                }), baseCurrency);

            return newRates.Concat(ratesInCache).ToList();
        }
    }
}
