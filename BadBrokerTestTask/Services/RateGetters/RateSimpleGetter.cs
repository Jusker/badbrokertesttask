﻿using BadBrokerTestTask.Models.UI;
using BadBrokerTestTask.Services.Integration;

namespace BadBrokerTestTask.Services.RateGetters
{
    /// <summary>
    /// Implementation <see cref="IRateSimpleGetter"/>
    /// </summary>
    public class RateSimpleGetter: IRateSimpleGetter
    {
        protected readonly IApilayerRateTimeseriesGetter _apilayerRateTimeseriesGetter;

        public RateSimpleGetter(IApilayerRateTimeseriesGetter apilayerRateTimeseriesGetter) {
            _apilayerRateTimeseriesGetter = apilayerRateTimeseriesGetter;
        }

        public async Task<List<Rate>> GetAsync(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols) {

            var timeseries = await _apilayerRateTimeseriesGetter.GetAsync(dateStart, dateEnd, baseCurrency, symbols);

            var rates = timeseries
                .Rates.Keys
                .SelectMany(e => timeseries.Rates[e].Keys, (a, b) =>  new Rate { 
                    DateTime = a,
                    Currency = b,
                    Value = timeseries.Rates[a][b]
            }).ToList();

            return rates;
        }
    }
}
