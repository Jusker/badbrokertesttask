﻿using BadBrokerTestTask.Models.UI;

namespace BadBrokerTestTask.Services.RateGetters
{
    /// <summary>
    /// Functionality for getting rate values by cache and api
    /// </summary>
    public interface IApiLayerWithCacheGetter : IRateGetter
    {
        Task<List<Rate>> GetAsync(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols);
    }
}
