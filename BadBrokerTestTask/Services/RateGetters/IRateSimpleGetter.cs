﻿namespace BadBrokerTestTask.Services.RateGetters
{
    /// <summary>
    /// Simple getting values from an external api
    /// </summary>
    public interface IRateSimpleGetter : IRateGetter { }
}
