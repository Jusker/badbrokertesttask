﻿using BadBrokerTestTask.Models.Configuration;
using BadBrokerTestTask.Models.UI.External;
using Microsoft.Extensions.Options;
using System.Text.Json;

namespace BadBrokerTestTask.Services.Integration
{
    public class ApilayerRateTimeseriesGetter : IApilayerRateTimeseriesGetter
    {
        private readonly ExternalAPI _configuration;
        private static string _dateFormat = "yyyy'-'MM'-'dd";
        private readonly static HttpClient _client = new HttpClient();

        public ApilayerRateTimeseriesGetter(IOptionsSnapshot<ExternalAPI> options)
        {
            _configuration = options.Value;
        }

        public async Task<Timeseries> GetAsync(DateTime startDate, DateTime endDate, string baseCurrency, IEnumerable<string> symbols)
        {
            if (symbols == null || !symbols.Any()) throw new ArgumentException("Symbols for search are not passed!");
            if (string.IsNullOrEmpty(baseCurrency)) throw new ArgumentException("Base currency for search are not passed!");

            string path = $"{_configuration.Url}?start_date={startDate.ToString(_dateFormat)}&end_date={endDate.ToString(_dateFormat)}&base={baseCurrency}&symbols={string.Join(",", symbols)}";

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, path))
            {
                requestMessage.Headers.Add("apikey", _configuration.ApiKey);

                using (var response = await _client.SendAsync(requestMessage))
                {

                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    var data = JsonSerializer.Deserialize<Timeseries>(responseBody, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    return data!;

                }
            }
        }
    }
}
