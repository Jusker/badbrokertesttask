﻿using BadBrokerTestTask.Models.UI.External;

namespace BadBrokerTestTask.Services.Integration
{
    public interface IApilayerRateTimeseriesGetter
    {
        Task<Timeseries> GetAsync(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols);
    }
}
