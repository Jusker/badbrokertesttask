﻿using BadBrokerTestTask.Models.UI.Calculators.BestResult;
using BadBrokerTestTask.Models.UI.Result;

namespace BadBrokerTestTask.Services.BestResult
{
    /// <summary>
    /// Functionality to get the best buy-sell value
    /// </summary>
    public interface IBestResultGetter
    {
        Task<BestInfoMatchResult> GetAsync(BestResutGetterViewModel model);
    }
}
