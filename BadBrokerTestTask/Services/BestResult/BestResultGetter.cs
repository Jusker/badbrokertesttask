﻿using BadBrokerTestTask.Models.Configuration;
using BadBrokerTestTask.Models.UI.Calculators;
using BadBrokerTestTask.Models.UI.Calculators.BestResult;
using BadBrokerTestTask.Models.UI.Result;
using BadBrokerTestTask.Services.Calculators.SignleCurrency;
using BadBrokerTestTask.Services.RateGetters;
using Microsoft.Extensions.Options;

namespace BadBrokerTestTask.Services.BestResult
{
    /// <summary>
    /// Implementation <see cref="IBestResultGetter"/>
    /// </summary>
    public class BestResultGetter : IBestResultGetter
    {
        private readonly ISingleCurrencyCalculator _singleCurrencyCalculator;
        private readonly IRateGetter _rateGetter;
        private readonly Currency _currency;

       
        public BestResultGetter(ISingleCurrencyCalculator singleCurrencyCalculator, IRateGetter rateGetter, IOptionsSnapshot<Currency> options) {
            _singleCurrencyCalculator = singleCurrencyCalculator;
            _rateGetter = rateGetter;
            _currency = options.Value;
        }


        public async Task<BestInfoMatchResult> GetAsync(BestResutGetterViewModel model) {

            //Based on the dictionary, the course values are mapped.
            //For each currency pair, the best buy/ sell set is calculated.
            //Take the best course

            var rates = await _rateGetter.GetAsync(model.DateBeg, model.DateEnd, _currency.Base, _currency.Pairs);

            var bestOfTheBest = _currency.Pairs.Select(pair => {
                
                var currencyRates = rates
                    .Where(e => e.Currency == pair)
                    .Select(rate => new SignleCurrencyModel 
                    {
                        Date = rate.DateTime,
                        Value = rate.Value,
                    }) ;
                var bestRate = _singleCurrencyCalculator.FindBestMatch(currencyRates, model.MoneyUsd, _currency.Tax);


                return new
                {
                    tool = pair,
                    bestRate = bestRate
                };
            })
                .OrderByDescending(e => e.bestRate.Revenue)
                .First();

            return new BestInfoMatchResult 
            { 
                Rates  = rates,
                BuyDate = bestOfTheBest.bestRate.BuyDate,
                SellDate = bestOfTheBest.bestRate.SellDate,
                Tool = bestOfTheBest.tool,
                Revenue = bestOfTheBest.bestRate.Revenue,
            };
        }
    }
}
