﻿using BadBrokerTestTask.DataBase.Models.DAL.Repository;
using BadBrokerTestTask.Models.Configuration;
using BadBrokerTestTask.Services.BestResult;
using BadBrokerTestTask.Services.Calculators.SignleCurrency;
using BadBrokerTestTask.Services.Integration;
using BadBrokerTestTask.Services.RateGetters;
using BadBrokerTestTask.Utils;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace BadBrokerTestTask.Settings
{
    public static class ServiceCollectionExtention
    {

        public static IServiceCollection AddBestResultGetter(this IServiceCollection services, IConfiguration configuration) {

            services.TryAddScoped<ISingleCurrencyCalculator, SingleCurrencyCalculator>();
            services.TryAddScoped<IRateGetter, ApiLayerWithCacheGetter>();
            services.AddApiLayerWithCacheGetter(configuration);
            services.TryAddScoped<IBestResultGetter, BestResultGetter>();

            return services;
        }

        public static IServiceCollection AddRateSimpleGetter(this IServiceCollection services, IConfiguration configuration) {

            services.AddApilayerRateTimeseriesGetter(configuration);
            services.TryAddScoped<IRateSimpleGetter, RateSimpleGetter>();
            return services;
        }

        public static IServiceCollection AddApilayerRateTimeseriesGetter(this IServiceCollection services, IConfiguration configuration) {

            services.Configure<ExternalAPI>(configuration);
            services.TryAddScoped<IApilayerRateTimeseriesGetter, ApilayerRateTimeseriesGetter>();
            return services;
        }



        public static IServiceCollection AddApiLayerWithCacheGetter(this IServiceCollection services, IConfiguration configuration)
        {
            services.TryAddScoped<IDbCacheGetter, DbCacheGetter>();
            services.TryAddScoped<IDbCacheCreator, DbCacheCreator>();
            services.AddRateSimpleGetter(configuration);
            services.TryAddScoped<IDateRangeJoiner, DateRangeJoiner>();

            return services;
        }


        public static IServiceCollection AddInternalService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ExternalAPI>(configuration.GetSection(nameof(ExternalAPI)));
            services.Configure<Currency>(configuration.GetSection(nameof(Currency)));

            services.AddBestResultGetter(configuration);
            return services;
        }
    }
}
