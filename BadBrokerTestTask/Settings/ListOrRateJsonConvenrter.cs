﻿using BadBrokerTestTask.Models.UI;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BadBrokerTestTask.Settings
{
    public class ListOrRateJsonConvenrter : JsonConverter<List<Rate>>
    {
        public override List<Rate>? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<List<Rate>>(ref reader, options);
        }

        public override void Write(Utf8JsonWriter writer, List<Rate> value, JsonSerializerOptions options)
        {
            var newObj = value
                .GroupBy(e => e.DateTime)
                .Select(e =>
                {
                    var dict = new Dictionary<string, string>(
                        e.Select(r => new KeyValuePair<string, string>(r.Currency, r.Value.ToString()))
                        );
                    
                    dict.Add("date", e.Key.ToString());

                    return dict;
                });

            var baseSerialize = JsonSerializer.Serialize(newObj, options);

            writer.WriteRawValue(baseSerialize);
        }
    }
}
