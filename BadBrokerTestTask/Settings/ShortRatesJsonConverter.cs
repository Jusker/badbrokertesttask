﻿using BadBrokerTestTask.Models.UI.External;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BadBrokerTestTask.Settings
{
    public class ShortRatesJsonConverter : JsonConverter<ShortRates>
    {
        public override ShortRates? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<ShortRates>(ref reader, options);
        }

        public override void Write(Utf8JsonWriter writer, ShortRates value, JsonSerializerOptions options)
        {
            var newObj = value
                .Select(e => {
                    var internalDict = e.Value.Select(s => new KeyValuePair<string, string>(s.Key, s.Value.ToString()))
                        .ToDictionary(t => t.Key.ToLower(), g => g.Value);

                    internalDict.Add("date", e.Key.ToString());

                    return internalDict;
                }).ToList();

            var baseSerialize = JsonSerializer.Serialize(newObj, options);

            writer.WriteRawValue(baseSerialize);
        }
    }
}
