﻿using BadBrokerTestTask.Models.UI.Calculators.BestResult;
using BadBrokerTestTask.Models.UI.Result;
using BadBrokerTestTask.Services.BestResult;
using Microsoft.AspNetCore.Mvc;

namespace BadBrokerTestTask.Controllers
{



    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RatesController : ControllerBase
    {
        private readonly IBestResultGetter BestResultGetter;
        private readonly ILogger<RatesController> _logger;

        public RatesController(IBestResultGetter bestResultGetter, ILogger<RatesController> logger)
        {

            BestResultGetter = bestResultGetter;
            _logger = logger;
        }


        /// <summary>
        /// Get the best buy/sell value
        /// </summary>
        /// <param name="startDate">The date of the beginning</param>
        /// <param name="endDate">End date</param>
        /// <param name="moneyUsd">Number of available currency</param>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server error</response>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<BestInfoMatchResult>> Best(DateTime startDate, DateTime endDate, decimal moneyUsd)
        {
            var parameters = new { startDate, endDate, moneyUsd };
            _logger.LogInformation("Call Best API", parameters);

            try
            {
                return await BestResultGetter.GetAsync(new BestResutGetterViewModel
                {
                    DateBeg = startDate,
                    DateEnd = endDate,
                    MoneyUsd = moneyUsd,
                });
            }
            catch (Exception ex) {
#if (DEBUG)
                _logger.LogError(ex, ex.Message, parameters);
                throw;
#endif

                return StatusCode(500, "An error occurred on the server.");
            }
        }
    }
}
