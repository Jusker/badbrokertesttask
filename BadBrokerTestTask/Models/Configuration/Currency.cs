﻿namespace BadBrokerTestTask.Models.Configuration
{
    /// <summary>
    /// Currency settings
    /// </summary>
    public class Currency
    {
        /// <summary>
        /// Base currency against which values are made
        /// </summary>
        public string Base { get; set; } = string.Empty;

        /// <summary>
        /// Currency pairs for comparison
        /// </summary>
        public List<string> Pairs { get; set; } = new List<string>();

        /// <summary>
        /// Broker tax
        /// </summary>
        public decimal Tax { get; set; }
    }
}
