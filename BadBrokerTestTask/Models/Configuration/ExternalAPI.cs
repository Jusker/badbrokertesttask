﻿namespace BadBrokerTestTask.Models.Configuration
{
    /// <summary>
    /// External API settings
    /// </summary>
    public class ExternalAPI
    {
        /// <summary>
        /// seccurity Key
        /// </summary>
        public string ApiKey { get; set; } = string.Empty;

        /// <summary>
        /// URL API
        /// </summary>
        public string Url { get; set; } = string.Empty;
    }
}
