﻿namespace BadBrokerTestTask.Models.UI.External
{
    public class ShortRates : Dictionary<DateTime, Dictionary<string, decimal>> { }

    public class Timeseries
    {
        public ShortRates Rates { get; set; } = new ShortRates();
    }
}
