﻿using BadBrokerTestTask.Settings;
using System.Text.Json.Serialization;

namespace BadBrokerTestTask.Models.UI.Result
{
    /// <summary>
    /// Information on the best match
    /// </summary>
    public class BestInfoMatchResult 
    {
        /// <summary>
        /// Buy date
        /// </summary>
        public DateTime BuyDate { get; set; }

        /// <summary>
        /// Sell date
        /// </summary>
        public DateTime SellDate { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        public string Tool { get; set; } = string.Empty;

        /// <summary>
        /// Profit
        /// </summary>
        public decimal Revenue { get; set; }

        /// <summary>
        /// Used to calculate values
        /// </summary>
        [JsonConverter(typeof(ListOrRateJsonConvenrter))]
        public List<Rate> Rates { get; set; } = new List<Rate>();
    }
}
