﻿namespace BadBrokerTestTask.Models.UI.Calculators
{
    public class BestMathInfo
    {
        public DateTime BuyDate { get; set; }
        public DateTime SellDate { get; set; }
        public decimal Revenue { get; set; } 
    }
}
