﻿namespace BadBrokerTestTask.Models.UI.Calculators.BestResult
{
    public struct BestResutGetterViewModel
    {
        public DateTime DateBeg { get; set; }
        public DateTime DateEnd { get; set; }
        public decimal MoneyUsd { get; set; }
    }
}
