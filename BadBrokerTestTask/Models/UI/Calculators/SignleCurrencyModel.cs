﻿namespace BadBrokerTestTask.Models.UI.Calculators
{
    public class SignleCurrencyModel
    {
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }
}
