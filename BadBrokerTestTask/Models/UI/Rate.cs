﻿namespace BadBrokerTestTask.Models.UI
{
    public class Rate
    {
        public DateTime DateTime { get; set; }
        public string Currency { get; set; }
        public decimal Value { get; set; }
    }
}
