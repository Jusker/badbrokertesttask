﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BadBrokerTestTask.DataBase.Migrations
{
    public partial class AddBaseCurrency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BaseCurrencyCurrencyId",
                table: "Caches",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Caches_BaseCurrencyCurrencyId",
                table: "Caches",
                column: "BaseCurrencyCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Caches_Currencies_BaseCurrencyCurrencyId",
                table: "Caches",
                column: "BaseCurrencyCurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Caches_Currencies_BaseCurrencyCurrencyId",
                table: "Caches");

            migrationBuilder.DropIndex(
                name: "IX_Caches_BaseCurrencyCurrencyId",
                table: "Caches");

            migrationBuilder.DropColumn(
                name: "BaseCurrencyCurrencyId",
                table: "Caches");
        }
    }
}
