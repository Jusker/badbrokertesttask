﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BadBrokerTestTask.DataBase.Migrations
{
    public partial class deleteCurrency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Caches_Currencies_BaseCurrencyCurrencyId",
                table: "Caches");

            migrationBuilder.DropForeignKey(
                name: "FK_Caches_Currencies_CurrencyId",
                table: "Caches");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropIndex(
                name: "IX_Caches_BaseCurrencyCurrencyId",
                table: "Caches");

            migrationBuilder.DropIndex(
                name: "IX_Caches_CurrencyId",
                table: "Caches");

            migrationBuilder.DropColumn(
                name: "BaseCurrencyCurrencyId",
                table: "Caches");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Caches");

            migrationBuilder.AddColumn<string>(
                name: "BaseCurrency",
                table: "Caches",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Caches",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BaseCurrency",
                table: "Caches");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Caches");

            migrationBuilder.AddColumn<int>(
                name: "BaseCurrencyCurrencyId",
                table: "Caches",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Caches",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    CurrencyId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CurrencyName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.CurrencyId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Caches_BaseCurrencyCurrencyId",
                table: "Caches",
                column: "BaseCurrencyCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Caches_CurrencyId",
                table: "Caches",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Caches_Currencies_BaseCurrencyCurrencyId",
                table: "Caches",
                column: "BaseCurrencyCurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Caches_Currencies_CurrencyId",
                table: "Caches",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
