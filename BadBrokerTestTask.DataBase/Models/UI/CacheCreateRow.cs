﻿namespace BadBrokerTestTask.DataBase.Models.UI
{
    public class CacheCreateRow
    {
        public DateTime DateTime { get; set; }
        public string Currency { get; set; } = string.Empty;
        public decimal Value { get; set; }
        public string BaseCurrency { get; set; } = string.Empty;
    }
}
