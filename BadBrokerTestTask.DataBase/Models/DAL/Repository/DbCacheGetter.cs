﻿using BadBrokerTestTask.DataBase.DbContexts;
using BadBrokerTestTask.DataBase.Models.DAL.DTO;

namespace BadBrokerTestTask.DataBase.Models.DAL.Repository
{

    public class DbCacheGetter: IDbCacheGetter
    {
        public List<Cache> GetExistsRows(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols) {

            using var db = new CacheContext();

            return db.Caches
                .Where(
                    e => e.Date >= dateStart && e.Date <= dateEnd && 
                    e.BaseCurrency == baseCurrency && 
                    symbols.Contains(e.Currency))
                .ToList();
        }
    }
}
