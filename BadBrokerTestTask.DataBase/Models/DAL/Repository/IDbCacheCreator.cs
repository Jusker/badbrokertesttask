﻿using BadBrokerTestTask.DataBase.Models.UI;

namespace BadBrokerTestTask.DataBase.Models.DAL.Repository
{
    public interface IDbCacheCreator
    {
        Task InsertAsync(IEnumerable<CacheCreateRow> mergeValues, string baseCurrency);
    }
}
