﻿using BadBrokerTestTask.DataBase.Models.DAL.DTO;

namespace BadBrokerTestTask.DataBase.Models.DAL.Repository
{
    public interface IDbCacheGetter
    {
        List<Cache> GetExistsRows(DateTime dateStart, DateTime dateEnd, string baseCurrency, IEnumerable<string> symbols);

    }
}
