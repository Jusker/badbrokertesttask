﻿using BadBrokerTestTask.DataBase.Models.DAL.DTO;
using BadBrokerTestTask.DataBase.DbContexts;
using BadBrokerTestTask.DataBase.Models.UI;

namespace BadBrokerTestTask.DataBase.Models.DAL.Repository
{
    public  class DbCacheCreator: IDbCacheCreator
    {
        public async Task InsertAsync(IEnumerable<CacheCreateRow> mergeValues, string baseCurrency) {

            using var db = new CacheContext();

            var caches = db.Caches
                .AddRangeAsync(mergeValues.Select(v => new Cache {  
                    BaseCurrency = baseCurrency,
                    Currency = v.Currency,
                    Date = v.DateTime,
                    Value = v.Value
                }));

            await caches;

            await db.SaveChangesAsync();

        }
    }
}
