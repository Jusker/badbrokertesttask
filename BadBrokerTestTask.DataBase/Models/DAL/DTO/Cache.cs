﻿namespace BadBrokerTestTask.DataBase.Models.DAL.DTO
{
    public class Cache
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Currency { get; set; } = null!;
        public string BaseCurrency { get; set; } = null!;
        public decimal Value { get; set; }

    }
}
