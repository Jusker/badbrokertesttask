﻿using BadBrokerTestTask.DataBase.Models.DAL.DTO;
using Microsoft.EntityFrameworkCore;

namespace BadBrokerTestTask.DataBase.DbContexts
{
    public class CacheContext: DbContext
    {
        public string DbPath { get; }

        public CacheContext()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = Path.Join(path, "cache_db.db");
        }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");

        public DbSet<Cache> Caches { get; set; }
    }
}
